// importing express to use the Router() method
const express = require('express');

// allows us to access our HTTP method routes
const router = express.Router();

// importing taskControllers
const userControllers = require('../controllers/userControllers');


router.post('/', userControllers.createUserController);

router.get('/', userControllers.getAllUserController);

// get and update username
router.put('/:id', userControllers.updateUserController);

router.get('/getSingleUser/:id', userControllers.getSingleUserController);

module.exports = router;
