// All packages/modules should be required at the top of the file to avoid tampering or errors

const express = require('express');

// A package that uses an ODM or object document mapper. It allows us to translate our JS objects into database documents for MongoDB. It allows connection and easier manipulation of our documents in MongoDB.
const mongoose = require('mongoose');

const app = express();

const port = 5000;

/*
	Syntax:
		mongoose.connect("<connectionStringFromMongoDBAtlas>", {
			useNewUrlParser : true,
			useUnifiedTopology: true
		})
*/

const taskRoutes = require('./routes/taskRoutes');
const userRoutes = require('./routes/userRoutes');

mongoose.connect("mongodb+srv://admin_deluvio:admin169@deluvio-batch-169.rcgxg.mongodb.net/task169?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection;



db.on('error', console.error.bind(console, 'Connection Error'));
db.once('open', () => console.log('Connected to MongoDB'));

// console.log(db)

//middlewares
app.use(express.json());
app.use('/tasks', taskRoutes);
app.use('/users', userRoutes);

// our server will use a middleware to group all task routes under /tasks
// all the endpoints in the taskRoutes file will start with /tasks

app.listen(port, () => console.log(`Server is running at port ${port}`));

